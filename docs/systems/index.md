# NERSC Systems

NERSC is one of the largest facilities in the world devoted to
providing computational resources for scientific computing.

## Perlmutter

[Perlmutter](perlmutter/index.md) is a HPE (Hewlett Packard
Enterprise) Cray EX supercomputer, named in honor of Saul Perlmutter,
an astrophysicist at Berkeley Lab who shared the 2011 Nobel Prize in
Physics for his contributions to research showing that the expansion
of the universe is accelerating.

Perlmutter, based on the HPE Cray Shasta platform, is a heterogeneous
system comprising both CPU-only and GPU-accelerated nodes, with a
performance of 3-4 times the previous Cori system.

Perlmutter consists of 1536 GPU accelerated nodes with 1 AMD Milan processor 
and 4 NVIDIA A100 GPUs, and 3072 CPU-only nodes with 2 AMD Milan processors. 

## Data transfer nodes

The [data transfer nodes](dtn/index.md) are NERSC servers dedicated to
performing transfers between NERSC data storage resources such as HPSS
and the NERSC Global File System (NGF), and storage resources at other
sites. These nodes are being managed (and monitored for performance)
as part of a collaborative effort between ESnet and NERSC to enable
high performance data movement over the high-bandwidth 100Gb ESnet
wide-area network (WAN).
