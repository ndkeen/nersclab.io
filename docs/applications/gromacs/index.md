# Gromacs

GROMACS is a versatile package to perform molecular dynamics, i.e. to
simulate the Newtonian equations of motion for systems with hundreds
to millions of particles.

It is primarily designed for biochemical molecules like proteins,
lipids and nucleic acids that have a lot of complicated bonded
interactions, but since GROMACS is extremely fast at calculating
nonbonded interactions (that usually dominate simulations) many groups
are also using it for research on non-biological systems,
e.g. polymers.  (From gromacs.org)

NERSC provides modules for [Gromacs](http://www.gromacs.org).

Use the `module avail` command to see what versions are available:

```bash
nersc$ module avail gromacs
```

## Example

See the [example jobs page](../../jobs/examples/index.md) for additional
examples and information about jobs.

### Perlmutter

```slurm
#!/bin/bash
#SBATCH --qos=regular
#SBATCH --time=01:00:00
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=64
#SBATCH --cpus-per-task=4
#SBATCH --constraint=cpu

module load gromacs
srun -n 128 mdrun_mpi_sp >& test.log
```

## Support

*  [Documentation](http://www.gromacs.org/)

!!! tip
    If after consulting with the above you believe there is an issue
    with the NERSC module, please file a [support
    ticket](https://help.nersc.gov).
