# Math Libraries

NERSC supports a wide range of math libraries. Many are available
as [modules](../../environment/index.md).

 * [FFTW](./fftw/index.md)
 * [GNU Science Library (GSL)](https://www.gnu.org/s/gsl/manual/gsl-ref.html)
 * LAPACK/ BLAS/ ScaLAPACK
    * [MKL](./mkl/index.md)
    * [LibSci](./libsci/index.md)
 * [Trillinos](https://trilinos.github.io)

Perlmutter has additional libraries that can be used on CPUs and
GPUs:

 * [AOCL (AMD Optimizing CPU
   Libraries)](https://developer.amd.com/amd-aocl/) (installation expected in near future)
 * Math libraries for NVIDIA GPUs: cuBLAS, cuSOLVER, cuSPARSE,
   cuFFT, cuFFTW, etc.

For information on the libraries, check the Perlmutter Readiness
page's [Libraries](../../performance/readiness.md#libraries) section.

In addition, documentation on AOCL is available from the [AMD
Optimizing CPU Libraries User
Guide](https://www.amd.com/content/dam/amd/en/documents/pdfs/developer/aocl/aocl-v4.0-ga-user-guide.pdf)
and the [AMD Random Number Generator
Library](https://www.amd.com/content/dam/amd/en/documents/pdfs/developer/aocl/amd-secure-random-number-generator-library-2.0-whitepaper.pdf).

Documentation is also available at
[NVIDIA's CUDA libraries page](https://docs.nvidia.com/cuda-libraries/index.html). Some
CUDA libraries relevant to HPC are:

 * [cuBLAS](https://docs.nvidia.com/cuda/cublas/index.html): CUDA
   Basic Linear Algebra Subroutine (BLAS) library
 * [cuSOLVER](https://docs.nvidia.com/cuda/cusolver/index.html):
   Library for decompositions and linear system solutions for both
   dense and sparse matrices
 * [cuSPARSE](https://docs.nvidia.com/cuda/cusolver/index.html):
   CUDA sparse matrix library
 * [cuFFT](https://docs.nvidia.com/cuda/cufft/index.html): CUDA
   Fast Fourier Transform
 * [cuRAND](https://docs.nvidia.com/cuda/curand/index.html): CUDA
   Random number generation library
 * [cuTENSOR](https://docs.nvidia.com/cuda/cutensor/index.html): A
   High-Performance CUDA Library For Tensor Primitives
 * [cuSPARSELt](https://docs.nvidia.com/cuda/cusparselt/index.html):
   A High-Performance CUDA Library for Sparse Matrix-Matrix
   Multiplication
