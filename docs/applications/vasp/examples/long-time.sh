#!/bin/bash
#SBATCH -N 1
#SBATCH -C cpu
#SBATCH -J vasp_job
#SBATCH -o %x-%j.out
#SBATCH -e %x-%j.err
#SBATCH --qos=debug_preempt
#SBATCH --comment=0:45:00
#SBATCH --time=0:05:00
#SBATCH --time-min=0:05:00
#SBATCH --signal=B:USR1@60
#SBATCH --requeue
#SBATCH --open-mode=append

## Notes on parameters above:
##
## '--qos=XX'      can be set to any of several QoS to which the user has access, which
##                 may include: regular, debug, shared, preempt, debug-preempt, premium,
##                 overrun, or shared-overrun.
## '--comment=XX'  is the total time that SUM of restarts can run (can be VERY LARGE).
## '--time=XX'     is the maximum time that individual restart can run. This MUST fit
##                 inside the queue limit for the QoS that you want to use
##                 (see https://docs.nersc.gov/jobs/policy/ for details).
## '--time-min=XX' is the minimum time that job can run before being preempted (using this
##                 option can make it easier for Slurm to fit into queue, possibly allowing
##                 job to start sooner). Omit this parameter unless running in either
##                 --qos=preempt or --qos=debug_preempt.
## '--signal=B:USR1@60' sends signal to begin checkpointing @XX seconds before end-of-job
##                 (set this large enough to have enough time to write checkpoint file(s)
##                  before time limit is reached; 60 seconds is usually enough).
## '--requeue'     specifies job is elegible for requeue in case of preemption.
## '--open-mode=append' appends contents of to the end of standard output and standard
##                 error files with successive requeues.

# Remove STOPCAR file so job isn't blocked
if [ -f "STOPCAR" ]; then
rm STOPCAR
fi

# Select VASP module of choice
module load vasp/5.4.4-cpu

# srun must execute in background and catch signal on wait command
# so ampersand ('&') is REQUIRED here
srun -n 128 -c 2 --cpu_bind=cores vasp_std &

# Put any commands that need to run to continue the next job (fragment) here
ckpt_vasp() {
set -x
restarts=`squeue -h -O restartcnt -j $SLURM_JOB_ID`
echo checkpointing the ${restarts}-th job

# Trim space from restarts variable for inclusion into filenames
restarts_num=$(echo $restarts | sed -e 's/^[ \t]*//')
echo "Restart number: ==${restarts_num}=="

# Terminate VASP at the next electronic step
echo LABORT = .TRUE. >STOPCAR

# Wait until VASP completes current step, then write WAVECAR file and quit
srun_pid=$(ps -fle | grep srun | head -1 | awk '{print $4}')
echo srun pid is $srun_pid
wait $srun_pid

# Copy CONTCAR to POSCAR and back up data from current run in each folder
folder="checkpt-$SLURM_JOB_ID-${restarts_num}"
mkdir $folder
echo "In directory $folder"

cp -p CONTCAR POSCAR
cp -p CONTCAR "$folder/POSCAR"
echo "CONTCAR copied."

cp -p OUTCAR "$folder/OUTCAR-${restarts_num}"
echo "OUTCAR copied."

cp -p OSZICAR "$folder/OSZICAR-${restarts_num}"
echo "OSZICAR copied."

# Back up the vasprun.xml file in the parent folder
cp -p vasprun.xml "vasprun-${restarts_num}.xml"
echo "vasprun.xml copied."

set +x
}

ckpt_command=ckpt_vasp

# The 'max_timelimit' is max time per individual job, in seconds
#     This line MUST be included !!!
#     This MUST match the value set for '#SBATCH --time=' above !!!
max_timelimit=300

# The 'ckpt_overhead' is the time reserved to perform the checkpoint step, in seconds
#     This MUST fit within the max_timelimit !!!
#     This should match the value set for '--signal=B:USR1@XX' above
ckpt_overhead=60

# Requeue the job if remaining time > 0
.  /global/common/sw/cray/cnl7/haswell/nersc_cr/19.10/etc/env_setup.sh
requeue_job func_trap USR1

wait
